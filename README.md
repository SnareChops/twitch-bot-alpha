# SnareChops Twitch Chat Bot Alpha

Welcome to the alpha program for the bot.
Please remember that this is a WORK IN PROGRESS and is not FINISHED or COMPLETE.
There will be more releases as time goes on that will fix issues, and release new experimental features.
This software is licensed under the MIT license which is included in the LICENSE.md file.

## How to setup

> Note: You really need to be technical for this and comfortable using a command line. If you are not,
> then this Alpha is not for you, please wait for the Beta.

These instructions will allow you to install the bot locally on your computer. You of course could follow the
same instructions to install on a server, that is your call to make.

### Windows Setup

> You **MUST** have Windows Professional version or better. There is a link for **Docker Toolbox** in the documentation
> that might allow you to get around this requirement, though that is completely untested.

1. Install Docker Desktop for Windows using the instructions found here https://docs.docker.com/docker-for-windows/install/

   - This will force you to create a Docker Hub account... It sucks they make you do this, but oh well...
   - Pay close attention to the instructions for enabling Hyper-V (You may need to enter your bios to make some changes)

1. Proceed to Bot Setup instructions below

### Mac OSX Setup

1. Install Docker Desktop for Mac as found here https://docs.docker.com/docker-for-mac/install/
1. Proceed to Bot Setup instructions below

### Linux Setup

1. Install docker from your distro's package manager
1. Install docker-compose from your distro's package manager
1. Proceed to Bot Setup instructions below

### Bot Setup

1. Complete the .env file in this repository
   - All fields should be in the format SOME_VARIABLE=some_value with the = touching both variable name and value. Spaces are NOT allowed

- BOT_USERNAME is whatever twitch user you would like the bot to use (This is a real account you must have created prior to this, and can be YOUR user if you would like)
- AUTH_TOKEN is a token for the BOT_USERNAME that you can generate here: https://twitchapps.com/tmi/
- CHANNEL_NAME is the name of your channel
- DB_PWORD is a password for root database access. Set this to anything - CROSSBAR_SECRET is a password for connection to the web socket. Set this to anything (This feature isn't used in this alpha)

1. Open a command line and navigate to this folder
1. Run `docker-compose up`
   - The first time you run this it will pull the docker images from the cloud.
   - After pulling the images the containers will start to spin up but fail miserably. This is expected. Allow them to continue until you see the line `[Note] mysqld: ready for connections.`
   - After you see the above message. Kill the process using Ctrl+C
1. Run `docker-compose down`
1. Run `docker-compose up` to start the stack again.
   - This time you should see `Connected to irc-ws.chat.twitch.tv:80` after a bit. This is the signal that everything was successful and the bot is now running.

### Starting and Stopping the bot

You can safely "turn off" the bot at any time by killing the running process using Ctrl+C in the command line. `docker-compose up` starts the stack and `docker-compose down` cleans up the stack after it has been killed. Your commands will remain saved.

## DB access

It is possible to access the database directly should you want to look around. Use any MySQL client you would like and connect it to
127.0.0.1:3307. Username and password is whatever you set in the .env file.

## Reporing Issues

Please report issues using https://gitlab.com/SnareChops/twitch-bot/issues/new
