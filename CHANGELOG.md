# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [0.2.0] 16 Mar 2019

Alpha 0.2.x Release

- Added !cooldown command `!cooldown !<command name> <time in seconds>`
- - Sets a cooldown for a command (spam prevention)
- Added !fightstats command `!fightstats`
- - Shows win / loss stats for fights participated in
- Added description feature for commands
- - Start any command with a ? instead of a ! to see a description of the command
- - Use `?<command name> <description text>` to update a description
- Added a cooldown setting for !fight (spam prevention)
- Changed commands to only recognized at the beginning of messages
- Added more helpful error messages to guide users through command usage

Bugfixes:

- Fixed an issue that prevented broadcaster not being considered a mod in their own channel
- Fixed an issue where users could !fight themselves
- Fixed an issue where commands that use @ in their targets would prevent them from being responded to
- Fixed an issue that prevented users without badges from using commands
- Fixed an issue with !disable being used without a command to disable

## [0.1.0] 22 Feb 2019

Alpha 0.1.x Release Includes

- Add / update simple response commands `!add !<command name> <command response>`
- Delete simple response commands `!del !<command name>`
- Disable response commands `!disable !<command name>`
- Enable disabled response commands `!enable !<command name>`
- Get a list of commands users can run `!commands`
- Simple coin flip game for viewers `!fight <username>`
- Simple random number generator `!roll` or `!roll <max>`
