## Alpha 0.2.0 update instructions

To update to Alpha 0.2.0 perform the following

1. `git pull` to update this repository
1. `docker-compose pull` to pull the new images
1. `FIGHT_COOLDOWN=60` has been added to the `.env` file. Update this number to control how many seconds users must wait before using the !fight command again
1. Run `docker-compose up` to start the new version of the bot

> See the CHANGELOG.md file for information on new features and bug fixes.
